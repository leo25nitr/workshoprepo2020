
import getpass
import os

import environ
import coral_django
env = environ.Env(
    # set casting, default value
    DEBUG=(bool, False)
)
# reading .env file
environ.Env.read_env(os.path.join(env.str('CORAL_ENV_PATH'), '.env'))
coral_django.env = env


def read_pwd_file():
    from coral_django.coral_engine.utils import decode_base64
    pwd_file_path = os.path.join(os.environ.get(env('DB_PWD_FILE_PATH')), ".coral", "key.properties")
    pwd_separator = ":"
    pwd_dict = {}
    try:
        with open(pwd_file_path) as f:
            for line in f:
                if pwd_separator in line:
                    # Find the name and value by splitting original config
                    name, value = line.split(pwd_separator, 1)
                    if value == '':
                        raise Exception('Password for [' + name + ']not Set')
                    elif name == 'CORAL-' + env('DB_CORAL_INSTANCE'):
                        pwd_dict['CORAL'] = decode_base64(value.strip())
                    elif name == 'MREX-' + env('DB_MREX_INSTANCE'):
                        pwd_dict['MREX'] = decode_base64(value.strip())
                    elif name == 'MORAG-' + env('DB_MORAG_INSTANCE'):
                        pwd_dict['MORAG'] = decode_base64(value.strip())
                    elif name == 'PLMANAGER-' + env('DB_PLMANAGER_INSTANCE'):
                        pwd_dict['PLMANAGER'] = decode_base64(value.strip())
                    elif name == 'AP-' + env('SRV_AP_INSTANCE'):
                        pwd_dict['AP'] = decode_base64(value.strip())
                    elif name == 'APSUM-' + env('SRV_AP_SUM_INSTANCE'):
                        pwd_dict['APSUM'] = decode_base64(value.strip())

    except IOError:
        raise Exception('Please Set your Passwords in key.properties ')
    finally:
        f.close()

    return pwd_dict


def validate_user():
    if os.name == 'nt' and coral_django.user_name.lower() != env.str('LOCAL_USER').lower():
        raise Exception('Logged in user (' + coral_django.user_name + ') and env.LOCAL_USER (' + env(
            'LOCAL_USER') + ') is not same. Please change the LOCAL_USER in .env file')


def initialize_base_config():
    # For linux set technical user
    coral_django.user_name = getpass.getuser().lower()
    coral_django.base_conf = {'report_id': 1}
    # Set password in Global Variable
    coral_django.pwd_dict = read_pwd_file()
    # Validate user configuration
    validate_user()


def init_log_path():
    if os.name == 'nt':
        log_dir = os.path.join(env('USERPROFILE'), env.str('CORAL_LOG_PATH'))
        report_path = os.path.join(env('USERPROFILE'), env.str('CORAL_MEDIA_PATH'))
    else:
        log_dir = env('CORAL_LOG_PATH')
        report_path = env.str('CORAL_MEDIA_PATH')

    log_file = os.path.join(log_dir, env.str('CORAL_LOG_FILE_NAME'))
    if not (os.path.exists(log_dir)):
        # Create directory
        os.makedirs(log_dir)
        open(log_file, 'w').close()
    elif not (os.path.exists(log_file)):
        # Create File
        open(log_file, 'w').close()

    if not (os.path.exists(report_path)):
        os.makedirs(report_path)

    return log_file, report_path
