
"""
Django settings for coral project.

"""

from coral_django.coral.server_startup import *

initialize_base_config()
env = coral_django.env

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

DEBUG = env.bool('DEBUG')

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = env.str('SECRET_KEY')
ALLOWED_HOSTS = env.str('ALLOWED_HOSTS')

ROOT_URLCONF = 'coral_django.components.urls'
WSGI_APPLICATION = 'coral.wsgi.application'

# Application definition
INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'coral_django.components'
    ]

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.RemoteUserMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]

AUTHENTICATION_BACKENDS = [env.str('COMAUS_BACKEND')]

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

DATABASES = {
    'default': {
        'ENGINE': 'coral_django.coral.oracle_coral',
        'NAME': env('DB_CORAL_HOST') + ':' + env('DB_CORAL_PORT') + '/' + env('DB_CORAL_NAME'),
        'USER': env('DB_CORAL_USER'),
        'PASSWORD': coral_django.pwd_dict['CORAL'],
    },
    'MREX': {
        'ENGINE': 'django.db.backends.oracle',
        'NAME': env('DB_MREX_HOST') + ':' + env('DB_MREX_PORT') + '/' + env('DB_MREX_NAME'),
        'USER': env('DB_MREX_USER'),
        'PASSWORD': coral_django.pwd_dict['MREX'],
    }
}

CACHES = {
    'default': {
        # 'BACKEND': 'django.core.cache.backends.memcached.MemcachedCache',
        # 'LOCATION': env('CACHE_LOCATION'),
        'BACKEND': 'django.core.cache.backends.db.DatabaseCache',
        'LOCATION': 'DJANGO_CACHE',
        'TIMEOUT': 60*60*1, # 1Hour
    }
}

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]


LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'Europe/Berlin'
USE_TZ = False

# For Translation
USE_I18N = False
# For Localized formatting
USE_L10N = True

STATIC_URL = '/static/'
STATIC_ROOT = 'staticfiles'

STATICFILES_DIRS = (
    os.path.join(BASE_DIR, 'static'),
)

MEDIA_URL = '/media/'
MEDIA_ROOT = os.path.join(env('CORAL_MEDIA_PATH'), 'media')

SESSION_COOKIE_AGE = 60 * 60 * env.int('SESSION_TIMEOUT_HR')
SESSION_EXPIRE_AT_BROWSER_CLOSE = True
SESSION_SAVE_EVERY_REQUEST = False

APP_LOG_FILE, APP_REPORT_PATH = init_log_path()

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'console': {
            'format': '%(name)-12s %(levelname)-8s %(message)s'
        },
        'file': {
            'format': '%(asctime)s %(levelname)-8s %(thread)s %(name)-12s %(message)s'
        }
    },
    'handlers': {
        'console': {
            'level': env.str('LOG_LEVEL'),
            'class': 'logging.StreamHandler',
            'formatter': 'console'
        },
        'file': {
            'level': env.str('LOG_LEVEL'),
            'class': 'logging.handlers.TimedRotatingFileHandler',
            'formatter': 'file',
            'filename': APP_LOG_FILE,
            'backupCount': 30,
            'when': 'midnight',
            'delay': True
        }
    },
    'loggers': {
        '': {
            'level': env.str('LOG_LEVEL'),
            'handlers': ['file', 'console']
        },
        'django': {
            'level': env.str('LOG_LEVEL'),
            'handlers': ['file', 'console'],
            'propagate': False
        },
        'django.request': {
            # Stop SQL debug from logging to main logger
            'level': env.str('LOG_LEVEL'),
            'handlers': ['file', 'console'],
            'propagate': False
        },
        'django.template': {
            'level': env.str('LOG_LEVEL'),
            'handlers': ['file', 'console'],
            'propagate': False
        },
    }
}



